# DKX/NetteGCloudTrace

Google cloud trace integration for Nette DI

## Installation

```bash
$ composer require dkx/nette-gcloud
$ composer require dkx/nette-gcloud-trace
```

## Usage

```yaml
extensions:
    gcloud: DKX\NetteGCloud\DI\GCloudExtension
    gcloud.trace: DKX\NetteGCloudTrace\DI\GCloudTraceExtension
```

Now you'll be able to simply inject the `Google\Cloud\Trace\TraceClient`.
