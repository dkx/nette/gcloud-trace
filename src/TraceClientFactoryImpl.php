<?php

declare(strict_types=1);

namespace DKX\NetteGCloudTrace;

use DKX\NetteGCloud\Credentials\CredentialsProvider;
use DKX\NetteGCloud\ProjectId\ProjectIdProvider;
use Google\Cloud\Trace\TraceClient;

final class TraceClientFactoryImpl implements TraceClientFactory
{
	private ProjectIdProvider $projectIdProvider;

	private CredentialsProvider $credentialsProvider;

	public function __construct(ProjectIdProvider $projectIdProvider, CredentialsProvider $credentialsProvider)
	{
		$this->projectIdProvider   = $projectIdProvider;
		$this->credentialsProvider = $credentialsProvider;
	}

	/**
	 * @codeCoverageIgnore
	 */
	public function create() : TraceClient
	{
		return new TraceClient([
			'projectId' => $this->projectIdProvider->getProjectId(),
			'keyFile' => $this->credentialsProvider->getCredentials(),
		]);
	}
}
