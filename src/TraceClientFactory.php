<?php

declare(strict_types=1);

namespace DKX\NetteGCloudTrace;

use Google\Cloud\Trace\TraceClient;

interface TraceClientFactory
{
	public function create() : TraceClient;
}
