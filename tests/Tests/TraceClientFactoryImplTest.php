<?php

declare(strict_types=1);

namespace DKXTests\NetteGCloudTrace\Tests;

use DKX\NetteGCloud\Credentials\CredentialsProvider;
use DKX\NetteGCloud\ProjectId\ProjectIdProvider;
use DKX\NetteGCloudTrace\TraceClientFactoryImpl;
use DKXTests\NetteGCloudTrace\TestCase;
use Mockery;
use function assert;

final class TraceClientFactoryImplTest extends TestCase
{
	public function testCreate() : void
	{
		$projectIdProvider = Mockery::mock(ProjectIdProvider::class)
			->shouldReceive('getProjectId')->andReturn('abcd')->getMock();
		assert($projectIdProvider instanceof ProjectIdProvider);

		$credentialsProvider = Mockery::mock(CredentialsProvider::class)
			->shouldReceive('getCredentials')->andReturn([])->getMock();
		assert($credentialsProvider instanceof CredentialsProvider);

		$factory = new TraceClientFactoryImpl($projectIdProvider, $credentialsProvider);
		$factory->create();
	}
}
